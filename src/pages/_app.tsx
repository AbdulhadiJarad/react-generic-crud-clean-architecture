import PostsDataSourceImpl from '@/Arch/data/Posts/datasourse/PostsDataSourseImpl';
import PostsRepositoryImpl from '@/Arch/data/Posts/repositories/PostsRepo';
import PostsModel from '@/Arch/domain/entity/Posts/Fetch/models/PostsModel';
import DeletePostsModel from '@/Arch/domain/entity/Posts/Delete/models/PostsModel';
import CreatePostsModel from '@/Arch/domain/entity/Posts/Create/models/PostsModel';
import UpdatePostModel from '@/Arch/domain/entity/Posts/Update/models/PostsModel';
import DeletePostsUseCase from '@/Arch/domain/interactors/posts/DeletePostsUseCase';
import CreatePostsUseCase from '@/Arch/domain/interactors/posts/CreatePostsUseCase';
import UpdatePostUseCase from '@/Arch/domain/interactors/posts/UpdatePostsUseCase';
import FetchPostsUseCase from '@/Arch/domain/interactors/posts/FetchPostsUseCase';
import type { AppProps } from 'next/app';
import TestViewModelImpl from './presentation/view-model/test/TestViewModelImpl';
import AuthComponent from './presentation/view/test/TestComponent';

export default function App({ Component, pageProps }: AppProps) {
  const dataSourse = new PostsDataSourceImpl();

  const authRepository = new PostsRepositoryImpl(dataSourse);
  // domain layer
  const authHolder = new PostsModel();
  const deletePostsModel = new DeletePostsModel();
  const createPostModel = new CreatePostsModel();
  const updatePostModel = new UpdatePostModel();
  const deletePostsUseCase = new DeletePostsUseCase(authRepository, deletePostsModel);
  const createPostsUseCase = new CreatePostsUseCase(authRepository, createPostModel);
  const updatePostUseCase = new UpdatePostUseCase(authRepository, updatePostModel);
  const loginUseCase = new FetchPostsUseCase(authRepository, authHolder);
  // view layer
  const authViewModel = new TestViewModelImpl(loginUseCase, deletePostsUseCase, authHolder, deletePostsModel, updatePostModel, updatePostUseCase, createPostModel, createPostsUseCase );

  return <AuthComponent testViewModel={authViewModel}  {...pageProps} />
}
