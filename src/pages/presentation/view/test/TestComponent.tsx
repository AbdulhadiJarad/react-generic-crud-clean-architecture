import TransitionsAddModal from '@/components/AddModal';
import CardUI from '@/components/Card';
import BasicSelect from '@/components/Dropdown';
import CircularIndeterminate from '@/components/Loading';
import TransitionsModal from '@/components/Modal';
import AddCircleIcon from '@mui/icons-material/AddCircle';
import {
  Box
} from '@mui/material';
import Button from '@mui/material/Button';
import React from 'react';
import TestViewModel from '../../view-model/test/TestViewModel';
import BaseView from '../BaseView';
import { styles } from './styles';

export interface TestComponentProps {
  testViewModel: TestViewModel;
}

export interface TestComponentState {
  posts: [];
  currentPage: Number
}

export default class AuthComponent extends React.Component<TestComponentProps, TestComponentState>
  implements BaseView {

  private testViewModel: TestViewModel;

  public constructor(props: TestComponentProps) {
    super(props);

    const { testViewModel } = this.props;
    this.testViewModel = testViewModel;

    this.state = {
      posts: testViewModel.posts,
      currentPage: 0,
      loading: true,
    };
  }

  public componentDidMount(): void {
    this.testViewModel.attachView(this);
    this.testViewModel.onChangeLimit(5)
  }

  public componentWillUnmount(): void {
    this.testViewModel.detachView();
  }

  public onViewModelChanged(): void {
    this.setState({
      posts: this.testViewModel.posts,
      currentPage: this.testViewModel.currentPage,
      loading: false
    });
  }

  public changeLimit = (number: Number) => {
    this.setState({
      ...this.state,
      loading: true
    })
    this.testViewModel.onChangeLimit(number);
  }


  public onSelectChange = (value: Number) => {
    this.setState({
      ...this.state,
      loading: true
    })
    this.changeLimit(value)
  }

  public deleteAction = (id: Number) => {
    this.setState({
      ...this.state,
      loading: true
    });
    this.testViewModel.deletePost(id);
  }

  public editAction = (id, title, body) => {
    this.setState({
      ...this.state,
      title,
      body,
      id
    })
  }

  public submitUpdate = ({ id, title, body }) => {
    this.setState({
      ...this.state,
      title: null,
      body: null,
      id: null,
      loading: true
    });

    this.testViewModel.updatePost(id, title, body);
  }

  public submitCreate = ({ title, body }) => {
    this.setState({
      ...this.state,
      isAdd: false,
      loading: true
    });

    this.testViewModel.createPost(title, body);
  }

  public openAddModal = () => {
    this.setState({
      ...this.state,
      isAdd: true
    })
  }

  public render(): JSX.Element {
    const {
      posts
    } = this.state;


    const goNext = () => {
      this.testViewModel.onPostsClick(true);
    }

    const goPrev = () => {
      this.testViewModel.onPostsClick(false);
    }

    if (this.state.loading) {
      return (<CircularIndeterminate />)
    }

    else return (
      <div style={styles.root}>
        <Button onClick={this.openAddModal} sx={{margin: '5px'}} color='primary' variant='contained' endIcon={<AddCircleIcon />}>
          Add Post
        </Button>
        <Box sx={styles.postsWrapper} >
          {!!posts && posts.map(post => <CardUI id={post.id} title={post.title} editAction={this.editAction} deleteAction={this.deleteAction} body={post.body} />)}
        </Box>
        {this.state.title && <TransitionsModal updatePost={this.submitUpdate} id={this.state.id} title={this.state.title} body={this.state.body} />}
        {this.state.isAdd && <TransitionsAddModal addPost={this.submitCreate} />}
        <Box sx={styles.controls}>
          <Button disabled={!this.state.currentPage} variant="contained" onClick={goNext}>Prev</Button>
          <BasicSelect onSelectChange={this.onSelectChange} />
          <Button variant="contained" onClick={goPrev}>Next</Button>
        </Box>
      </div>
    );
  }
}
