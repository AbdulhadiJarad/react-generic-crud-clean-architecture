export const styles: any = {
  root: { margin: "0px auto", width: "100%" },
  postsWrapper: {
    display: "flex",
    flexWrap: "wrap",
    width: 1,
    gap: "50px",
    margin: "0px auto",
    justifyContent: "flex-start",
  },
  controls: {
    margin: "0px auto",
    textAlign: "center",
    display: "flex",
    justifyContent: "center",
    gap: 4,
    height: "40px",
    marginTop: '50px'
  },
};