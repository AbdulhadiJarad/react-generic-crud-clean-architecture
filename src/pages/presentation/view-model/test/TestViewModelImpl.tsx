import TestViewModel from './TestViewModel';
import BaseView from '../../view/BaseView';
import FetchPostsUseCase from '@/Arch/domain/interactors/posts/FetchPostsUseCase';
import PostsListener from '@/Arch/domain/entity/Posts/Fetch/models/PostsListener';
import PostsModel from '@/Arch/domain/entity/Posts/Fetch/models/PostsModel';
import DeletePostModel from '@/Arch/domain/entity/Posts/Delete/models/PostsModel';
import CreatePostModel from '@/Arch/domain/entity/Posts/Create/models/PostsModel';
import UpdatePostModel from '@/Arch/domain/entity/Posts/Update/models/PostsModel';
import DeletePostsUseCase from '@/Arch/domain/interactors/posts/DeletePostsUseCase';
import CreatePostsUseCase from '@/Arch/domain/interactors/posts/CreatePostsUseCase';
import UpdatePostUseCase from '@/Arch/domain/interactors/posts/UpdatePostsUseCase';

export default class TestViewModelImpl implements TestViewModel, PostsListener {

  private baseView?: BaseView;
  private fetchPostsUseCase: FetchPostsUseCase;
  private updatePostUseCase: UpdatePostUseCase;
  private deletePostsUseCase: DeletePostsUseCase;
  private createPostsUseCase: CreatePostsUseCase;
  private postsModel: PostsModel;
  private deletePostModel: DeletePostModel;
  private createPostModel: CreatePostModel;
  private updatePostModel: UpdatePostModel;
  public posts: any
  public currentPage: any;

  public constructor(fetchPostsUseCase: FetchPostsUseCase, deletePostsUseCase: DeletePostsUseCase, postsModel: PostsModel, deletePostModel: DeletePostModel, updatePostModel: UpdatePostModel, updatePostUseCase: UpdatePostUseCase, createPostModel: CreatePostModel, createPostsUseCase: CreatePostsUseCase ) {
    this.posts = [];
    this.currentPage = 0;
    this.fetchPostsUseCase = fetchPostsUseCase;
    this.postsModel = postsModel;
    this.deletePostsUseCase = deletePostsUseCase;
    this.updatePostUseCase = updatePostUseCase;
    this.createPostsUseCase = createPostsUseCase;
    this.deletePostModel = deletePostModel;
    this.updatePostModel = updatePostModel;
    this.createPostModel = createPostModel;
    this.createPostModel.addPostsListener(this);
    this.postsModel.addPostsListener(this);
    this.updatePostModel.addPostsListener(this);
    this.deletePostModel.addPostsListener(this);
  }

  public attachView = (baseView: BaseView): void => {
    this.baseView = baseView;
  };

  public detachView = (): void => {
    this.baseView = undefined;
  };

  public onPostDeleted = (): void => {
    console.log('Done');
    this.notifyViewAboutChanges();
  }

  public onPostUpdated = (): void => {
    console.log('Done');
    this.notifyViewAboutChanges();
  }

  public onPostCreated = (): void => {
    console.log('Done');
    this.notifyViewAboutChanges();
  }

  public onPostsChanged = () => {
    this.posts = this.postsModel.posts;
    this.notifyViewAboutChanges();
  }

  public deletePost = async (id: Number) => {
    try {
      await this.deletePostsUseCase.deletePost(id);
      this.posts = this.posts.filter(e => e.id != id);
      this.notifyViewAboutChanges();
    } catch (e) {
      console.log('error', e)
    }
  }

  public createPost = async (title: String, body: String) => {
    try {
      await this.createPostsUseCase.createPost(title, body);
      console.log('done')
    } catch (e) {
      console.log('error', e)
    }
  }

  public updatePost = async (id: Number, title: String, body: String) => {
    try {
      await this.updatePostUseCase.updatePost(id, title, body);
      this.notifyViewAboutChanges();
    } catch (e) {
      console.log('error', e)
    }
  }

  public onChangeLimit = async (number: Number): Promise<void> => {
    try {
      await this.fetchPostsUseCase.getPosts(0, number);
      this.notifyViewAboutChanges();
    } catch (e) {
      console.log('error', e)
    }
  };

  public onPostsClick = async (isPrev: Boolean): Promise<void> => {
    try {

      if (!isPrev)
        this.currentPage++;
      else
        this.currentPage--;

      await this.fetchPostsUseCase.getPosts(this.currentPage, 5);

      this.notifyViewAboutChanges();
    } catch (e) {
      console.log('error', e)
    }
  };

  private notifyViewAboutChanges = (): void => {
    if (this.baseView) {
      this.baseView.onViewModelChanged();
    }
  };
}
