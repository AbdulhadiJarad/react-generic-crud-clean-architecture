import BaseViewModel from '../BaseViewModel';

export default interface TestViewModel extends BaseViewModel {
  posts: any;

  onPostsClick(isPrev: Boolean): any;
  onChangeLimit(number: Number): any;
  updatePost(id: Number, title: String, body: String): any;
  createPost(title: String, body: String): any;
  deletePost(id: Number): any;
}
