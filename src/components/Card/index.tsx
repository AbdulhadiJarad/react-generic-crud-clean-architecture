import { CardActionArea } from '@mui/material';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import DeleteIcon from '@mui/icons-material/Delete';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import EditIcon from '@mui/icons-material/Edit';

export default function CardUI({ title, id, body, deleteAction, editAction }) {
    return (
        <Card sx={{ flexBasis: '20%', padding: '10px' }}>
            <CardActionArea>
                <CardContent>
                    <Box sx={{display: 'flex', justifyContent: 'space-between', marginBottom: '10px' }} >
                        <Button onClick={() => deleteAction(id)} endIcon={<DeleteIcon/>} sx={{width: '90px', height: '30px', textTransform: 'capitalize' }} color='error' variant='contained' >
                        Delete
                        </Button>
                        <Button onClick={() => editAction(id, title, body)} endIcon={<EditIcon />} sx={{ width: '90px', height: '30px', textTransform: 'capitalize' }} color='secondary' variant='contained' >
                            Edit
                        </Button>
                    </Box>
                    <Typography gutterBottom variant="h5" component="div">
                        {title}
                    </Typography>
                    <Typography variant="body2" color="text.secondary">
                        {body}
                    </Typography>
                </CardContent>
            </CardActionArea>
        </Card>
    );
}
