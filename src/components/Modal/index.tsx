import * as React from 'react';
import Backdrop from '@mui/material/Backdrop';
import Box from '@mui/material/Box';
import Modal from '@mui/material/Modal';
import Fade from '@mui/material/Fade';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import { TextField } from '@mui/material';

const style = {
    position: 'absolute' as 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: 'background.paper',
    border: '2px solid #000',
    boxShadow: 24,
    p: 4,
};

export default function TransitionsModal({updatePost, id, title, body}) {
    const [open, setOpen] = React.useState(true);
    const handleOpen = () => setOpen(true);
    const handleClose = () => setOpen(false);
    const [form, setForm] = React.useState({ title, body });
    const onChange = e => {
        setForm({
            ...form,
            [e.target.name]: e.target.value
        });
    }

    

    return (
        <div>
            <Button onClick={handleOpen}>Open modal</Button>
            <Modal
                aria-labelledby="transition-modal-title"
                aria-describedby="transition-modal-description"
                open={open}
                onClose={handleClose}
                closeAfterTransition
                slots={{ backdrop: Backdrop }}
                slotProps={{
                    backdrop: {
                        timeout: 500,
                    },
                }}
            >
                <Fade in={open}>
                    <Box sx={style}>
                        <TextField name='title' 
                            onChange={onChange}
                         fullWidth id="outlined-basic" defaultValue={title} label="Outlined" variant="outlined" />
                        <TextField
                            fullWidth
                            sx={{marginTop:'10px'}}
                            id="outlined-multiline-static"
                            label="Multiline"
                            multiline
                            rows={4}
                            name={'body'}
                            onChange={onChange}
                            defaultValue={body}
                        />
                        <Button sx={{ marginTop: '10px' }} fullWidth color='primary' variant='contained' onClick={() => updatePost({ ...form, id })}>
                            Edit Post
                        </Button>
                    </Box>
                </Fade>
            </Modal>
        </div>
    );
}
