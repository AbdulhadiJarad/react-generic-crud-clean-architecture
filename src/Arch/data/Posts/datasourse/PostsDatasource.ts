import ValidationResult from '../../../domain/entity/Posts/Fetch/stuctures/PostsValidationResult';

export default abstract class PostsDataSource {
  /**
   * @throws {Error} if validation has not passed
   */
  abstract getPosts(page: Number, limit: Number): Promise<ValidationResult>;
  abstract deletePost(id: Number): Promise<ValidationResult>;
  abstract updatePost(id: Number, title: String, body: String): Promise<ValidationResult>;
  abstract createPost(title: String, body: String): Promise<ValidationResult>;
}
