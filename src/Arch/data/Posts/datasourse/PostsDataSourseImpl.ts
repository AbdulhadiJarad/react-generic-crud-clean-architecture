import ValidationResult from "@/Arch/domain/entity/Posts/Fetch/stuctures/PostsValidationResult";
import { apiUrl } from "@/Services/Endpoints";
import PostsDataSource from "./PostsDatasource";
import axios from "axios";


export default class PostsDataSourceImpl extends PostsDataSource {
  /**
   * @throws {Error} if validation has not passed
   */

  getPosts(page: Number, limit: Number): Promise<ValidationResult> {
    return axios({
      method: "get",
      url: `${apiUrl}posts?_start=${page}&_limit=${limit}`,
      headers: { "Content-Type": "application/json" },
    });
  }

  deletePost(id: Number): Promise<ValidationResult> {
    return axios({
      method: "delete",
      url: `${apiUrl}posts/${id}`,
      headers: { "Content-Type": "application/json" },
    });
  }

  updatePost(id: Number, title: String, body: String): Promise<ValidationResult> {
    return axios({
      method: "PATCH",
      url: `${apiUrl}posts/${id}`,
      data: JSON.stringify({ id, title, body }),
      headers: { "Content-Type": "application/json" },
    });
  }

  createPost(title: String, body: String): Promise<ValidationResult> {
    return axios({
      method: "POST",
      url: `${apiUrl}posts`,
      data: JSON.stringify({ title, body }),
      headers: { "Content-Type": "application/json" },
    });
  }
}
