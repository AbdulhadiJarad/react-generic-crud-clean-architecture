import PostsRepository from '../../../domain/repository/Posts/PostsRepo';
import ValidationResult, { ValidationListResultEntity, ValidationResultEntity } from '../../../domain/entity/Posts/Fetch/stuctures/PostsValidationResult';
import PostsDataSource from '../datasourse/PostsDatasource';

export default class PostsRepositoryImpl implements PostsRepository {
  /**
   * @throws {Error} if validation has not passed
   */
  private postsDatasourse: PostsDataSource;

  constructor(postsDatasourse: PostsDataSource) {
    this.postsDatasourse = postsDatasourse;
  }

  async getPosts(page: Number, limit: Number): Promise<ValidationListResultEntity> {
    var payload = await this.postsDatasourse.getPosts(page, limit);
    const data = payload.data;
    return JSON.parse(JSON.stringify(data));
  }

  async deletePost(id: Number): Promise<ValidationListResultEntity> {
    var payload = await this.postsDatasourse.deletePost(id);
    const data = payload.data;
    return JSON.parse(JSON.stringify(data));
  }

  async updatePost(id: Number, title: String, body: String): Promise<ValidationListResultEntity> {
    var payload = await this.postsDatasourse.updatePost(id, title, body);
    const data = payload.data;
    return JSON.parse(JSON.stringify(data));
  }

  async createPost(title: String, body: String): Promise<ValidationListResultEntity> {
    var payload = await this.postsDatasourse.createPost(title, body);
    const data = payload.data;
    return JSON.parse(JSON.stringify(data));
  }
}