import ValidationResult from '../../entity/Posts/Fetch/stuctures/PostsValidationResult';

export default interface CountriesRepository {
  /**
   * @throws {Error} if validation has not passed
   */
  getPosts(page: Number, limit: Number): Promise<ValidationResult>;
  deletePost(id: Number): Promise<ValidationResult>;
  updatePost(id: Number, title: String, body: String): Promise<ValidationResult>;
  createPost(title: String, body: String): Promise<ValidationResult>;
}
