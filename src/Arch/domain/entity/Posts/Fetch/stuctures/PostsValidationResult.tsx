export interface ValidationListResultEntity {
  data: Array<ValidationResultEntity>;
}

export default interface ValidationResult {
  data:Array<any>;
}


export interface ValidationResultEntity {
  userId: Number,
  id: Number,
  title: String,
  body: String,
}

