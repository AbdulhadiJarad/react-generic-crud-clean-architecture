import { ValidationListResultEntity } from '../stuctures/PostsValidationResult';
import PostsListener from './PostsListener';

export default class PostsUIModel {
  private postsListeners: PostsListener[];
  public posts: any;

  public constructor() {
    this.postsListeners = [];
    this.posts = Array<ValidationListResultEntity>;
  }

  public onPostUpdated(posts: ValidationListResultEntity): void {
    this.notifyListeners();
  }

  public addPostsListener(postsListener: PostsListener): void {
    this.postsListeners.push(postsListener);
  }

  public removePostsListener(postsListener: PostsListener): void {
    this.postsListeners.splice(this.postsListeners.indexOf(postsListener), 1);
  }

  private notifyListeners(): void {
    this.postsListeners.forEach((listener) => listener.onPostUpdated());
  }
}
