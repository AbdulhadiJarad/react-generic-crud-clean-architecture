import PostsRepository from '../../repository/Posts/PostsRepo';
import PostsModel from '../../entity/Posts/Delete/models/PostsModel';

export default class DeletePostsUseCase {
    private postsRepository: PostsRepository;
    private postsModel: PostsModel

    public constructor(postsRepository: PostsRepository, postsModel: PostsModel) {
        this.postsRepository = postsRepository;
        this.postsModel = postsModel;
    }

    /**
     * @throws {Error} if credentials are not valid or have not passed
     */
    public async deletePost(id: Number): Promise<void> {
        const postsData = await this.postsRepository.deletePost(id);
        this.postsModel.onPostDeleted();
    }
}
