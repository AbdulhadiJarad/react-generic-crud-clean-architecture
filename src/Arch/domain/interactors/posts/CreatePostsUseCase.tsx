import PostsRepository from '../../repository/Posts/PostsRepo';
import PostsModel from '../../entity/Posts/Create/models/PostsModel';

export default class CreatePostsUseCase { private postsRepository: PostsRepository;
  private postsModel: PostsModel

  public constructor(postsRepository: PostsRepository, postsModel: PostsModel) {
    this.postsRepository = postsRepository;
    this.postsModel = postsModel;
  }

  /**
   * @throws {Error} if credentials are not valid or have not passed
   */
  public async createPost(title: String, body: String): Promise<void> {
    const postsData = await this.postsRepository.createPost(title, body);
    this.postsModel.onPostCreated(postsData);
  }
}
