import PostsRepository from '../../repository/Posts/PostsRepo';
import PostsModel from '../../entity/Posts/Update/models/PostsModel';

export default class FetchPostsUseCase { private postsRepository: PostsRepository;
  private postsModel: PostsModel

  public constructor(postsRepository: PostsRepository, postsModel: PostsModel) {
    this.postsRepository = postsRepository;
    this.postsModel = postsModel;
  }

  /**
   * @throws {Error} if credentials are not valid or have not passed
   */
  public async updatePost(id: Number, title: String, body: String): Promise<void> {
    const postsData = await this.postsRepository.updatePost(id, title, body);
    this.postsModel.onPostUpdated(postsData);
  }
}
