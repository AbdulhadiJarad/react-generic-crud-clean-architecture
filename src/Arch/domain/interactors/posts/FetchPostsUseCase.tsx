import PostsRepository from '../../repository/Posts/PostsRepo';
import PostsModel from '../../entity/Posts/Fetch/models/PostsModel';

export default class FetchPostsUseCase { private postsRepository: PostsRepository;
  private postsModel: PostsModel

  public constructor(postsRepository: PostsRepository, postsModel: PostsModel) {
    this.postsRepository = postsRepository;
    this.postsModel = postsModel;
  }

  /**
   * @throws {Error} if credentials are not valid or have not passed
   */
  public async getPosts(page: Number, limit: Number): Promise<void> {
    const postsData = await this.postsRepository.getPosts(page, limit);
    this.postsModel.onPostsFetched(postsData);
  }
}
